import { Component, OnInit, Input, OnChanges, ViewChild, ElementRef  } from '@angular/core';
import { ICountryCovidData, IGlobalCovidData, ICovidSummary } from '../../models/IWeatherData.interface';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css'],
  providers: [DatePipe]
})
export class CityComponent implements OnInit, OnChanges {

  @Input() covidsummary: ICovidSummary;
  @Input() countryfilter: string;
  globaldetails: IGlobalCovidData;
  public countrydetails;
  date: string;
  dataSource: MatTableDataSource<ICountryCovidData>;
  myDate = new Date();

  displayedColumns = [
    'Country Code', 'Country Name', 'New Confirmed', 'Total Confirmed',
    'New Deaths', 'Total Deaths', 'New Recovered', 'Total Recovered'
  ];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


constructor(private datePipe: DatePipe) {

  this.date = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
}

ngOnInit() {
}

ngOnChanges() {
      this.countrydetails = this.covidsummary.Countries;
      this.globaldetails = this.covidsummary.Global;
      this.date = this.covidsummary.Date;
      const country = this.countrydetails.filter((value) => {
        return value.CountryCode.indexOf(this.countryfilter) !== -1 ? value : null;
      });
      this.dataSource = new MatTableDataSource(country);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
}
}
