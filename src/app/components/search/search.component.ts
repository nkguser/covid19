import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { ICountries } from '../../models/IWeatherData.interface';
import countries from '../../_file/countries.json';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

    @ViewChild('searchInput') searchInput;
    @Output() selectedCity: EventEmitter<string> = new EventEmitter();

    CountriesList: ICountries[];
    searchResults: ICountries[] = null;

    constructor() { }
    ngOnInit() {
      this.CountriesList = countries;
    }


    search(event: any) {

      if (event.target.value.length > 2) {
        this.searchResults = this.filterResults(event.target.value);
      } else if (event.target.value.length === 0) {
        this.searchResults = null;
      }
    }

    private filterResults(keyword: string): Array<ICountries> {
      let searchItemByCode = [];
      let searchItemByName = [];

      let result = [];
      /// filter by code
      searchItemByCode = this.CountriesList ? this.CountriesList
                                                  .filter(item => item.ISO2.search(new RegExp(keyword, 'i')) > - 1) : [];

      /// filter by name
      searchItemByName = this.CountriesList ? this.CountriesList
                                                  .filter(item => item.Country.search(new RegExp(keyword, 'i')) > - 1) : [];

      result = searchItemByCode.concat(searchItemByName);
      result = Array.from(new Set(result.map(s => s.ISO2)))
        .map(ISO2 => {
          return {
            ISO2,
            Country: result.find(s => s.ISO2 === ISO2).Country
          };
        });
      return result;
    }


    selectedLocation(countryDetails: ICountries) {
       this.searchInput = ' ';
       this.searchResults = null;
       this.selectedCity.emit(countryDetails.ISO2);
    }
}
