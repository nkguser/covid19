export interface ICovidSummary {
    Global: IGlobalCovidData;
    Countries: ICountryCovidData[];
    Date: string;
}

export interface ICountries {
    Country: string;
    Slug: string;
    ISO2: string;
}

export interface IGlobalCovidData {
  NewConfirmed: number;
  TotalConfirmed: number;
  NewDeaths: number;
  TotalDeaths: number;
  NewRecovered: number;
  TotalRecovered: number;
}

export interface ICountryCovidData {
  Country: string;
  CountryCode: string;
  Slug: string;
  NewConfirmed: number;
  TotalConfirmed: number;
  NewDeaths: number;
  TotalDeaths: number;
  NewRecovered: number;
  TotalRecovered: number;
  Date: string;
}
