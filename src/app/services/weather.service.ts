import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import { ICovidSummary } from '../models/IWeatherData.interface';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) {

   }

  baseUrl = 'https://api.covid19api.com';

  getCityDetails(): Observable<ICovidSummary> {
   const urls = '/summary';
   return this.http.get<ICovidSummary>(urls).pipe(retry(1), catchError(this.handleError));
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  // transformRawData(rawData: IGlobalCovidData) {
  //   const transformedData: Array<ICountryCovidData> = [];

  //   rawData.consolidated_weather.forEach(function(obj) {
  //     const date = obj.applicable_date;
  //     const temperature = obj.the_temp;
  //     const weather_name = obj.weather_state_name;
  //     const weather_image = 'https://www.metaweather.com/static/img/weather/' + obj.weather_state_abbr + '.svg';

  //     transformedWeather.push({date, temperature, weather_name, weather_image } as ICityWeather);
  //   });

  //   return {
  //     city: rawData.title,
  //     country: rawData.parent.title,
  //     weather: transformedWeather
  //   };
  // }
}
