import { Component, OnInit } from '@angular/core';
import { WeatherService } from './services/weather.service';
import { ICountries, IGlobalCovidData, ICountryCovidData, ICovidSummary } from './models/IWeatherData.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Covid 19 Tracker App';
  Countryfilter: string;
  covidsummary: ICovidSummary;
  globaldetails: IGlobalCovidData;
  date: string;


  constructor(private weatherService: WeatherService) {}

  ngOnInit() {

  }
  getCityDetails(ISO2: string) {
    this.weatherService.getCityDetails().subscribe((Globalresp: ICovidSummary) => {
    this.covidsummary = Globalresp;
    this.Countryfilter = ISO2;
  });
  }
}
